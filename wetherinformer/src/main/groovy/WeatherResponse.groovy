import groovyx.net.http.HTTPBuilder

class WeatherResponse {
    String weatherUrl
    Map weatherQuery
    String path
    String city
    Map responseWeather

    WeatherResponse(typeOfRequest, city) {
        this.weatherUrl = "http://api.openweathermap.org"
        this.path = "/data/2.5/" + typeOfRequest
        this.weatherQuery = ['APPID': "b2ce5b9466a4cdcec5e7a6bf11465c5a", 'units': "metric"]
        this.city = city
        this.responseWeather = [:]
    }

    def getWeatherResponse() {
        weatherQuery.put('q', city)
        try {
            def http = new HTTPBuilder(weatherUrl)
            responseWeather = http.get(path : path, query : weatherQuery)

        } catch (Exception ex) {
            ex.printStackTrace()
        }
    }

    def parseForcast5Days(){
        println("Прогноз погоды")
        println("Город: " + responseWeather.city.name + " Страна: " + responseWeather.city.country)
        responseWeather.list.each {
            println("----------------------------------")
            println("Дата прогноза: " + it.dt_txt)
            println("Температура: " + it.main.temp + " min: " + it.main.temp_min + " max: " + it.main.temp_max)
            println("Давление: " + it.main.pressure + " гПа")
            println("Влажность: " + it.main.humidity + " %")
            println("Облачность: " + it.clouds.all + " %")
            println("Осадки: " + it.weather.description)
            println("Скорость ветра: " + it.wind.speed + " м/с")
            println("Направление ветра: " + it.wind.deg)

        }
    }
}
